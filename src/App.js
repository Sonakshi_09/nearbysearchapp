import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import MapContain from './components/Mapcontain';
import  SearchBox from './components/searchbox'

import { Provider } from 'react-redux';
import store from './redux/store'

class App extends Component{
  render(){
    return(
      <Provider store={store}>
      <div classname="App">
        <header classname="App-header">
        </header>
      <div classname="container h-100">
      {/* <MapContainer /> */}
      <SearchBox />
      <MapContain />
      </div>
      <div>

        </div>
    </div>
    </Provider>
    );
  }
}


export default App;
