import { SET_POSITION } from './actiontypes'


const initialstate = { 
    posvalues: ''

}
const reducer = ( state = initialstate, action) => {
    console.log("payload", action.payload);

    switch(action.type){
        case SET_POSITION : return{
            ...state, 
            posvalues: state.posvalues
        }
        default : return state
    }

}

export default reducer