import {createStore} from 'redux'
import reducer from './reducer'
import { get_coords } from './action'
const store = createStore(reducer)


console.log("Initial state", store.getState());
store.subscribe(()=> console.log('State updated',store.getState()));
store.dispatch(get_coords());
export default store