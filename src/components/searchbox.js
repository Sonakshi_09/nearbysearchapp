import React from 'react'
import { Segment, Input } from 'semantic-ui-react'
import axios from 'axios'



var map;
var service;
var infowindow;
var google;

export default class SearchBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            places:''
        };
      }
render(){

function initialize(props) {
  var pyrmont = new google.maps.LatLng(28.7041,77.1025);

  map = new google.maps.Map(document.getElementById('map'), {
      center: pyrmont,
      zoom: 15
    });

  var request = {
    location: pyrmont,
    radius: '500',
    type: ['restaurant']
  };

  service = new google.maps.places.PlacesService(map);
  service.nearbySearch(request, getresults);
}

function getresults(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
}
function createMarker(place) {
    const marker = new google.maps.Marker({
      map,
      position: place.geometry.location,
    });
    google.maps.event.addListener(marker, "click", () => {
      infowindow.setContent(place.name);
      infowindow.open(map);
    });
  }
        return(
                <Segment inverted>
                  <Input inverted placeholder='Search google maps...'  onClick={ getresults } />
                  <Input inverted placeholder='Set Radius...' />
                </Segment>
                            
        );
    }
}

// const getresults =() =>{
        //     const URL = `https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${
        //         28.7041},${77.1025}&type=${'restaurant'}&radius=${1000}&key='AIzaSyBHpzxtPZef39oeGQcWUR5U7P1Acf4UHn8`;
        //       axios
        //         .get(URL)
        //         .then(response => {
        //           this.places = response.results;
        //           this.addLocationsToGoogleMaps();
        //         })
        //         .catch(error => {
        //           console.log(error.message);
        //         });
        // } 